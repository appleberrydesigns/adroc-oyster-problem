package com.appleberrydesigns.adrocoysterproblem.service;

import static org.junit.jupiter.api.Assertions.*;

import com.appleberrydesigns.adrocoysterproblem.exception.CardException;
import com.appleberrydesigns.adrocoysterproblem.model.Bus;
import com.appleberrydesigns.adrocoysterproblem.model.Fare;
import com.appleberrydesigns.adrocoysterproblem.model.Station;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


class CardServiceTest {

    private CardService cardService;
    private final Long testCardId = CardService.TEST_CARD_ID;
    private final BigDecimal amount = BigDecimal.valueOf(10);

    @BeforeEach
    void setUp() {
        cardService = new CardService();
    }

    @Test
    @DisplayName("Topping up card should set correct balance")
    void topUpCard() throws CardException {
        cardService.topUpCard(testCardId, amount);
        assertEquals(amount, cardService.getCardBalance(testCardId));
    }

    @Test
    @DisplayName("Swiping into station should charge max fare")
    void swipeIntoStation() throws CardException {
        cardService.topUpCard(testCardId, amount);
        cardService.swipeIntoStation(testCardId, Station.HOLBORN);
        assertEquals(amount.subtract(Fare.MAX_FARE.amount), cardService.getCardBalance(testCardId));
    }

    @Test
    @DisplayName("Swiping into station with low balance should throw exception")
    void exceptionTesting() {
        Throwable exception = assertThrows(CardException.class, () -> cardService.swipeIntoStation(testCardId, Station.HOLBORN));
        assertEquals("Not enough money on card. Balance is £0", exception.getMessage());
    }

    @Test
    @DisplayName("Swiping out of station should refund max fare and calculate real fare")
    void swipeOutOfStation() throws CardException{
        cardService.topUpCard(testCardId, amount);
        cardService.swipeIntoStation(testCardId, Station.HOLBORN);
        cardService.swipeOutOfStation(testCardId, Station.WIMBLEDON);
        assertEquals(amount.subtract(Fare.THREE_ZONES.amount), cardService.getCardBalance(testCardId));
    }

    @Test
    @DisplayName("Swiping onto bus should charge bus fare")
    void swipeOntoBus() throws CardException {
        cardService.topUpCard(testCardId, amount);
        cardService.swipeOntoBus(testCardId, Bus.NO_328);
        assertEquals(amount.subtract(Fare.BUS.amount), cardService.getCardBalance(testCardId));
    }
}
