package com.appleberrydesigns.adrocoysterproblem;

import com.appleberrydesigns.adrocoysterproblem.model.Bus;
import com.appleberrydesigns.adrocoysterproblem.model.Station;
import com.appleberrydesigns.adrocoysterproblem.service.CardService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.math.BigDecimal;

@SpringBootApplication
public class AdrocOysterProblemApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdrocOysterProblemApplication.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            // Initialise
            System.out.println("");
            System.out.println("Initialising Application");
            System.out.println("------------------------");
            System.out.println("");

            CardService cardService = new CardService();
            Long userCardId = CardService.TEST_CARD_ID;

            // User loads card with £30
            cardService.topUpCard(userCardId, BigDecimal.valueOf(30));

            // User travels from Holborn tube to Earl's Court tube
            cardService.swipeIntoStation(userCardId, Station.HOLBORN);
            cardService.swipeOutOfStation(userCardId, Station.EARLS_COURT);

            // User travels on 328 bus
            cardService.swipeOntoBus(userCardId, Bus.NO_328);

            // User travels from Earl's Court tube to Hammersmith tube
            cardService.swipeIntoStation(userCardId, Station.EARLS_COURT);
            cardService.swipeOutOfStation(userCardId, Station.HAMMERSMITH);

            // User checks card balance
            BigDecimal balance = cardService.getCardBalance(userCardId);
            System.out.println("");
            System.out.println("------------------------");
            System.out.println("Balance is: £" + balance.toString());
            System.out.println("------------------------");
        };
    }
}
