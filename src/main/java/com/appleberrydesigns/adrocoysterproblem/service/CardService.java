package com.appleberrydesigns.adrocoysterproblem.service;

import com.appleberrydesigns.adrocoysterproblem.exception.CardException;
import com.appleberrydesigns.adrocoysterproblem.model.Bus;
import com.appleberrydesigns.adrocoysterproblem.model.Card;
import com.appleberrydesigns.adrocoysterproblem.model.Fare;
import com.appleberrydesigns.adrocoysterproblem.model.Station;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class CardService {
    private final Map<Long, Card> cards = new HashMap<>();

    public static final Long TEST_CARD_ID = 123L;

    public CardService() {
        // Mock DB
        Card card = new Card(TEST_CARD_ID, BigDecimal.valueOf(0));
        cards.put(card.getId(), card);
    }

    private void addMoneyToCard(Card card, BigDecimal amount) {
        card.setBalance(card.getBalance().add(amount));
        System.out.println("£" + amount + " added to card " + card.getId());
    }

    private void subtractMoneyFromCard(Card card, BigDecimal amount) throws CardException {
        BigDecimal currentBalance = card.getBalance();
        if (amount.compareTo(currentBalance) > 0) {
            throw new CardException("Not enough money on card. Balance is £" + currentBalance.toString());
        }
        card.setBalance(card.getBalance().subtract(amount));
        System.out.println("£" + amount + " subtracted from card " + card.getId());
    }

    private void refundMaxFare(Card card) {
        addMoneyToCard(card, Fare.MAX_FARE.amount);
    }

    private Fare calculateFare(List<Integer> startZone, List<Integer> endZone) {
        // Find common zones
        Set<Integer> commonZones = startZone.stream()
                .distinct()
                .filter(endZone::contains)
                .collect(Collectors.toSet());

        // No common zones, i.e. travel across multiple zones
        if (commonZones.isEmpty()) {
            int zonesTravelled = Collections.min(endZone) - Collections.max(startZone) + 1;

            if (zonesTravelled == 3) {
                return Fare.THREE_ZONES;
            }

            if (zonesTravelled == 2) {
                if (startZone.contains(1) || endZone.contains(1)) {
                    return Fare.TWO_ZONES_INC_ZONE_ONE;
                } else {
                    return Fare.TWO_ZONES_EXC_ZONE_ONE;
                }
            }
        } else { // Travel in one zone only
            if (commonZones.contains(1)) {
                return Fare.ZONE_ONE_ONLY;
            } else {
                return Fare.ONE_ZONE_OUTSIDE_ZONE_ONE;
            }
        }

        // Unknown journey so charge max fare
        return Fare.MAX_FARE;
    }

    public void swipeIntoStation(Long cardId, Station station) throws CardException {
        System.out.println("Start journey at " + station.name);
        Card card = cards.get(cardId);
        card.setStartZone(station.zones);
        subtractMoneyFromCard(card, Fare.MAX_FARE.amount);
    }

    public void swipeOutOfStation(Long cardId, Station station) throws CardException {
        System.out.println("End journey at " + station.name);
        Card card = cards.get(cardId);
        refundMaxFare(card);
        Fare fare = calculateFare(card.getStartZone(), station.zones);
        subtractMoneyFromCard(card, fare.amount);
    }

    public void swipeOntoBus(Long cardId, Bus bus) throws CardException {
        System.out.println("Start bus journey on No. " + bus.name);
        Card card = cards.get(cardId);
        subtractMoneyFromCard(card, Fare.BUS.amount);
    }

    public void topUpCard(Long cardId, BigDecimal amount) throws CardException {
        if (amount.compareTo(BigDecimal.valueOf(0)) < 0) {
            throw new CardException("Cannot add negative amount to card.");
        }
        Card card = cards.get(cardId);
        addMoneyToCard(card, amount);
    }

    public BigDecimal getCardBalance(Long cardId) {
        return cards.get(cardId).getBalance();
    }
}
