package com.appleberrydesigns.adrocoysterproblem.model;

import java.util.List;

public enum Station {
    HOLBORN("Holborn", List.of(1)),
    EARLS_COURT("Earl's Court", List.of(1, 2)),
    WIMBLEDON("Wimbledon", List.of(3)),
    HAMMERSMITH("Hammersmith", List.of(2));

    public final String name;
    public final List<Integer> zones;

    Station(String name, List<Integer> zones) {
        this.name = name;
        this.zones = zones;
    }
}
