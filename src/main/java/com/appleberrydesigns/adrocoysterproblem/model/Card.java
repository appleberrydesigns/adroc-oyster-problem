package com.appleberrydesigns.adrocoysterproblem.model;

import java.math.BigDecimal;
import java.util.List;

public class Card {
    private final long id;
    private BigDecimal balance;
    private List<Integer> startZone;

    public Card(long id, BigDecimal balance) {
        this.id = id;
        this.balance = balance;
    }

    public Long getId() {
        return this.id;
    }

    public BigDecimal getBalance() {
        return this.balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public List<Integer> getStartZone() {
        return this.startZone;
    }

    public void setStartZone(List<Integer> zone) {
        this.startZone = zone;
    }
}
