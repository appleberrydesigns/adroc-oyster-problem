package com.appleberrydesigns.adrocoysterproblem.model;

import java.math.BigDecimal;

public enum Fare {

        ZONE_ONE_ONLY(BigDecimal.valueOf(250,2)),
        ONE_ZONE_OUTSIDE_ZONE_ONE(BigDecimal.valueOf(200,2)),
        TWO_ZONES_INC_ZONE_ONE(BigDecimal.valueOf(300,2)),
        TWO_ZONES_EXC_ZONE_ONE(BigDecimal.valueOf(225,2)),
        THREE_ZONES(BigDecimal.valueOf(320,2)),
        BUS(BigDecimal.valueOf(180,2)),
        MAX_FARE(BigDecimal.valueOf(320,2));

        public final BigDecimal amount;

        Fare(BigDecimal amount) {
            this.amount = amount;
        }
}
