package com.appleberrydesigns.adrocoysterproblem.model;

public enum Bus {
    NO_328("328");

    public final String name;

    Bus(String name) {
        this.name = name;
    }
}
